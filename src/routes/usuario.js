const express = require("express")
const router = express.Router();
const Usuario = require ("../models/usuario.model")

router.get("/mostrar", (req, res)=>{
    Usuario.find()
    .then((usuarios)=>{
        res.json(usuarios);
    })
    .catch(err=> console.log(err))

});

router.post("/agregar", (req, res)=>{
    const nombre = req.body.nomApe;
    const cedula = req.body.ci;
    const direccion = req.body.direccion;
    usuariosAlquiler = new Usuario({
        nomApe: nombre,
        ci: cedula,
        direccion: direccion
        
    })
    usuariosAlquiler.save()
    .then(usuario=>{
        res.json(usuario)
    })
    .catch(err=> console.log(err))
})

router.put("/guardar/:id", (req, res) =>{
    let id= req.params.id;
    Usuario.findById(id)
        .then(usuario=>{
            usuario.nombre = req.body.nomApe;
            usuario.ci = req.body.ci;
            usuario.descripcion = req.body.descripcion;
            usuario.save()
                .then(usuario=>{
                    res.send({
                        menssage:"Guardado exitosamente",
                        status: "success",
                        usuario:usuario
                    })
                })
                .catch(err=> console.log(err))
        })
        .catch(err => console.log(err))
})

router.delete("/eliminar/:id", (req ,res)=>{
    let id = req.params.id;
    Usuario.findById(id)
        .then(usuario=>{
            usuario.delete()
                .then(usuario=>{
                    res.send({
                        menssage: "Eliminada exitosamente",
                        status:"success",
                        usuario:usuario
                    })
                })
                .catch(err=> console.log(err))
        })
        .catch(err=> console.log(err))
})

module.exports = router