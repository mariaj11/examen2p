const request = require('supertest')
const app = require("/Users/DELL/Documents/aULEAM/a-Sexto Semestre/Aplicaciones Web II/Semana 7/examen2p/index");
//const { deleteOne } = require('../models/usuario.models');


test("Respuesta", async () => {
    await request(app)
        .get("/index/usuario")
})

describe("/GET /index/usuarios/mostrar", () => {
    test("Respuesta", async () => {
        await request(app)
            .get("/index/usuarios/mostrar")
    })

})

describe("/POST /index/usuarios/agregar", () => {
    test("Datos de usuarios registrados correctamente", async () => {
        await request(app).post("/index/usuarios/agregar")
            .send({
                nomApe: "Cedeño Alvia",
                ci: "1317863247",
                direccion: "Los algarrobos"

            })
            .catch((err) => (err))
    })

})

describe("/PUT /index/usuarios/guardar/:id", () => {
    test("Usuario modificado correctamente en la base de datos ", async () => {
        await request(app).put("/index/usuarios/guardar/60006f9bf9b9fb19304ac417")
            .send({
                nomApe: "Cedeño Cevallos",
                ci: "1317863247",
                direccion: "Los algarrobos"

            })
            .catch((err) => (err))
    })
})


