const mongoose = require('mongoose');

const {Schema}=mongoose

const usuarioSchema = new Schema(
    {
        nomApe: {type:String},        
        ci:{type:String},
        direccion:{type:String}
    },
    {
        timestamps:true
    }
)

const Usuario = mongoose.model("usuario", usuarioSchema)
module.exports = Usuario;