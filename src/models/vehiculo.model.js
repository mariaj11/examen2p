const mongoose = require('mongoose');

const {Schema}=mongoose

const vehiculoSchema = new Schema(
    {
        idvehiculo:{type:String},
        nomVehiculo:{type:String},        
        tiempo:{type:String},
        tipolicencia:{type:String}
    },
    {
        timestamps:true
    }
)

const Vehiculo = mongoose.model("Vehiculo", vehiculoSchema)
module.exports = Vehiculo;