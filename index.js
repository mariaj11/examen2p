//Librerias instaladas
const app = require('express')();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require("cors");
//Librerias locales
const { MONGO_URI } = require('./src/config/index');
const usuarioRoutes = require ('./src/routes/usuario')

//Conectarnos a nuestras base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true }).then (p=>{
    console.log('Conexion exitosa');
}).catch(err=>{
    console.log(err);
});

//Puertoo
app.listen(5000);
app.get("/", (req, res)=>{
    res.send("Hola word");
})

//formato
app.use(cors());
app.use(bodyParser.json());


app.use("/index/usuarios", usuarioRoutes);

//Telegram
require('./src/telegram');

module.exports =app 
